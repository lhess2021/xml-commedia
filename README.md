# xml-commedia

Storage for XML files used to represent Dante's *Divine Comedy*. Also includes text files and programs used to generate the XML.

This repo is mirrored at my home server: `https://git.lorenzohess.net:1534/lhess/xml-commedia-mirror`.

## Plaintext to XML
I use Python and the Bourne Again Shell (Bash) to convert the *Comedy* from plaintext to XML. Currently, the XML *Comedy* complies with TEI specifications (full teiHeader coming soon). I took the plaintext files from this project: [ginestra/dante-visualized](https://github.com/ginestra/dante-visualised). The plaintext files are stored under `comedy-plaintext.d`, which contains subfolders for each canticle. Each subfolder contains text files, one per canto.

### `scripts.d/py-scripts/parser-txt-to-xml.py`
This script does the text to XML conversion. The XML looks like the following:
```xml
<?xml version="1.0" encoding="UTF-8"?>

<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <text>
  <canto canticle="inferno" c_num="1">
   <tercets>
    <tercet t_num="1">
     <l l_num="1"> <w>Nel</w> <w>mezzo</w> <w>del</w> <w>cammin</w> <w>di</w> <w>nostrdjkl</w> <w>vita</w> </l>
     <l l_num="2"> <w>mi</w> <w>ritrovai</w> <w>per</w> <w>una</w> <w>selva</w> <w>oscura</w> </l>
     <l l_num="3"> <w>ché</w> <w>la</w> <w>diritta</w> <w>via</w> <w>era</w> <w>smarrita</w>. </l>
    </tercet>
    <tercet t_num="2">
     <l l_num="4"> <w>Ahi</w> <w>quanto</w> <w>a</w> <w>dir</w> <w>qual</w> <w>era</w> <w>è</w> <w>cosa</w> <w>dura</w> </l>
     <l l_num="5"> <w>esta</w> <w>selva</w> <w>selvaggia</w> <w>e</w> <w>aspra</w> <w>e</w> <w>forte</w> </l>
     <l l_num="6"> <w>che</w> <w>nel</w> <w>pensier</w> <w>rinova</w> <w>la</w> <w>paura</w>! </l>
    </tercet>
    ...
    <tercet t_num="45">
     <l l_num="133"> <w>che</w> <w>tu</w> <w>mi</w> <w>meni</w> <w>là</w> <w>dov’or</w> <w>dicesti</w>, </l>
     <l l_num="134"> <w>sì</w> <w>ch’io</w> <w>veggia</w> <w>la</w> <w>porta</w> <w>di</w> <w>san</w> <w>Pietro</w> </l>
     <l l_num="135"> <w>e</w> <w>color</w> <w>cui</w> <w>tu</w> <w>fai</w> <w>cotanto</w> <w>mesti</w>». </l>
    </tercet>
    <tercet t_num="46">
     <l l_num="136"> <w>Allor</w> <w>si</w> <w>mosse</w>, <w>e</w> <w>io</w> <w>li</w> <w>tenni</w> <w>dietro</w>. </l>
    </tercet>
   </tercets>
  </canto>
 </text>
</TEI>
```
