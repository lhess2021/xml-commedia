import shutil
import os
import re

cantoDir = os.path.abspath(os.getcwd())

def main():
    """
    Take txt file from `dante-visualized` project and remove blank lines
    """
    for txtFile in os.listdir(cantoDir):
        if re.search('\.txt$', txtFile):
            tmpFile = '.' + txtFile+ '.tmp'
            with open(txtFile, 'r') as txt:
                with open(tmpFile, 'w') as tmp:
                    for line in txt:
                        if line.rstrip():
                            tmp.write(line)
                tmp.close()
            txt.close()
            shutil.copy(tmpFile, txtFile)
            os.remove(tmpFile)

main()
