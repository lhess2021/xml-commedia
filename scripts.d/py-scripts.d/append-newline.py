"""
Append "\n" to the canto text files in Purgatorio and Paradiso, because for some reason,
likely due to the formatting ginestra/dante-visualized used, they don't have at trailing
newline character, which messes up my parser.
"""

import os
import re

cantoDir = os.path.abspath(os.getcwd())

def appendNewline(cantoFile):
    with open(cantoFile, 'a') as textFile:
        textFile.write("\n")
    textFile.close()

def main():
    for canto in os.listdir(cantoDir):
        if re.search('\.txt$', canto):
            appendNewline(canto)


main()
