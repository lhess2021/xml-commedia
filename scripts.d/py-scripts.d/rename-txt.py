import re
import os

canto_dir = os.path.abspath(os.getcwd())
canto_files = os.listdir(canto_dir)

def rename_canto(canto):
    """
    Take txt canto files from `dante-visualized` and change the name styling
    """
    # parse canto name
    # put it back together
    print('canto name: ' + canto)
    new_canto = re.sub('\_','-',canto)
    new_canto = re.sub('Canto[IVX]*-','',new_canto)
    new_canto = new_canto.lower()
    print('--> new canto name: ' + new_canto)
    os.rename(canto,new_canto)

for canto in canto_files:
    if canto != 'rename-txt.py' and canto != '.rename-txt.py.swp':
        rename_canto(canto)
