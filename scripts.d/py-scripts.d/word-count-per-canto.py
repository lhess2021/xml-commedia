import re
import os
import glob
import shutil

"""
generate CSV files for each word in the comedy, containing three columns: canticle,canto,word count of specific word in canto
run from py-scripts.d
"""

currentDir = os.path.abspath(os.getcwd())
csvDir = '../../comedy-wordcount.d/csv/'

def wordCountPerCanto(csvDir):
    """
    should be run after word-count-generator.py
    iterates through inferno-num, purgatorio-num, paradiso-num CSV dirs
    """
    wordsList = list()
    subDirs = [csvDir + 'inferno-num/', csvDir + 'purgatorio-num/', csvDir + 'paradiso-num/']
    canticleMatch = 'inferno|purgatorio|paradiso'
    for subDir in subDirs:
        print('iterating through ' + subDir)
        for i in range(1, 34):
            canto = str(i).zfill(3)
            canticle = re.search(canticleMatch, subDir).group(0)
            csvFile = subDir + canto + '-' + canticle + '-petrocchi-num.csv'
            #print('csv file: ' + csvFile)
            canto = str(int(canto))
            #print(canticle + ' ' + canto + '\n')
            with open(csvFile, 'r') as csvFile:
                for line in csvFile:
                    word = line.split(',')[0]
                    count = line.split(',')[1]
                    wordCsvFile = word + '.csv'
                    if word in wordsList:
                        with open(wordCsvFile, 'a') as wordCsvFile:
                            csvInput = canticle + ',' + canto + ',' + count
                            wordCsvFile.write(csvInput)
                            wordCsvFile.close()
                    else:
                        with open(wordCsvFile, 'w') as wordCsvFile:
                            csvInput = canticle + ',' + canto + ',' + count 
                            wordCsvFile.write(csvInput)
                            wordCsvFile.close()
                        wordsList.append(word)
                csvFile.close()

def makeDirs():
    # moves *.csv to ../../comedy-wordcount.d/csv/word-per-canto/
    csvFiles = glob.iglob(os.path.join(currentDir, '*.csv'))
    wordPerCantoDir = '../../comedy-wordcount.d/csv/word-per-canto/'
    for csvFile in csvFiles:
        shutil.move(csvFile, wordPerCantoDir)

def main():
    wordCountPerCanto(csvDir)
    makeDirs()


main()
