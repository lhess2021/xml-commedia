#!/bin/bash

# An interactive xpath query commandline tool.
# Stores your previous query to reduce typing.

# File for previous query
touch .prev-xpath.tmp
if [[ ! -e ".gitignore" ]]; then
	echo 'xpath.sh' > .gitignore
fi

# Colored output
GREEN='\033[0;32m'
NC='\033[0m' # no color

# Ask for canto file
clear
read -p "Choose a canto: [i,r,p][1-34] " CANTO
canticle="$(echo $CANTO | tr -d -c a-z)"
canto_num="$(echo $CANTO | tr -d -c 0-9)"
pad_num="$(printf "%03d\n" $canto_num)"

# Create complete filename
if [[ "$canticle" == 'i' ]]; then
	canticle='Inferno'
	file="$pad_num-inferno-petrocchi.xml"
	echo $file
elif [[ "$canticle" == 'r' ]]; then
	canticle='Purgatorio'
	file="$pad_num-purgatorio-petrocchi.xml"
elif [[ "$canticle" == 'p' ]]; then
	canticle='Paradiso'
	file="$pad_num-paradiso-petrocchi.xml"
else
	echo "Not a valid canto file: $file"
fi

# Start interactive query
if [[ -e "$file" ]]; then
	clear && echo -e "You are searching ${canticle^} ${CANTO_NUM}.\nAdd a trailing slash ${GREEN}\`/\`${NC} to your search before submitting it. The default first two search choices are ${GREEN}\`canto\`${NC} and then ${GREEN}\`terzine\`${NC}. Press ${GREEN}<enter>${NC} for those defaults and type something else to search that.\n"
	read -p "Load previous: (Y)/n " PREV
	case "$PREV" in
		N|n)
			search=''
			;;
		Y|y|*)
			search="$(cat .prev-xpath.tmp)"
			;;
	esac
	i='0'
	# Start while loop for the query. Break the loop if the query ends with a slash `/`.
	while true; do
		read -e -i "$search" -p "$(echo -e "\nSearch: \n  "${GREEN}"===> "${NC})" nsearch
		if [[ "${nsearch: -1}" == / ]]; then		# if last char trailing slash then search
			path="${nsearch::-1}"			# remove trailing slash
			echo "$path" > .prev-xpath.tmp		# store query for next time
			# perform the search (requires `xmlstarlet` and `bat` pager)
			xmlstarlet sel -t -v "$path" $file | batcat -l xml
			exit
		elif [[ "$i" == 0 ]] && [[ "$nsearch" == '' ]]; then 	# load '/TEI/text' query on first loop
			search="_:TEI/_:text/_:"
		elif [[ "$nsearch" == '_:TEI/_:text/_:' ]]; then			# load '/TEI/text/canto/tercets' on second loop
			search="_:TEI/_:text/_:canto/_:tercets/_:"
		else
			search="${nsearch}"				# else keep search the same
		fi
		i=$[$i+1]
	done	
else
	echo "Canto file not found. What directory are you in?"
fi
